       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MY-GRADE.
       AUTHOR. NATCHA.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE VALUE HIGH-VALUE.
           02 SUB-ID      PIC 9(6).
           02 SUB-NAME    PIC X(50).
           02 CREDIT      PIC 9.
           02 GRADE       PIC X(2).

       FD  AVG-FILE.
       01  AVG-DETAIL.
           02 AVG-GRADE      PIC 9V999.
           02 AVG-SCI-GRADE  PIC 9V999.
           02 AVG-CS-GRADE   PIC 9V999.
           

       WORKING-STORAGE SECTION.
       01  NUM-GRADE      PIC 999V999.
       01  SUM-GRADE      PIC 999V999.
       01  SUM-SCI-GRADE  PIC 999V999.
       01  SUM-CS-GRADE   PIC 999V999.
       01  SUM-CREDIT     PIC 999 VALUE ZEROS.
       01  SUM-SCI-CREDIT PIC 999 VALUE ZEROS.
       01  SUM-CS-CREDIT  PIC 999 VALUE ZEROS.
       01  FIRST-SUB-ID-NUM PIC X.
       01  TWO-SUB-ID-NUM   PIC X(2).
           

       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 
           OPEN OUTPUT AVG-FILE 
           PERFORM  UNTIL END-OF-GRADE-FILE 
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE
                 PERFORM 002-AVG-CAL THRU 002-EXIT
              END-READ
              IF NOT END-OF-GRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF
            END-PERFORM 
           CLOSE AVG-FILE 
           CLOSE GRADE-FILE
           GOBACK  
           .
       001-PROCESS.
           EVALUATE TRUE 
              WHEN GRADE = "A" MOVE 4 TO NUM-GRADE
              WHEN GRADE = "B+" MOVE 3.5 TO NUM-GRADE
              WHEN GRADE = "B" MOVE 3.0 TO NUM-GRADE
              WHEN GRADE = "C+" MOVE 2.5 TO NUM-GRADE
              WHEN GRADE = "C" MOVE 2.0 TO NUM-GRADE
              WHEN GRADE = "D+" MOVE 1.5 TO NUM-GRADE
              WHEN GRADE = "D" MOVE 1.0 TO NUM-GRADE
              WHEN OTHER MOVE 0 TO NUM-GRADE
           END-EVALUATE 

           MOVE SUB-ID TO FIRST-SUB-ID-NUM 
           MOVE SUB-ID TO TWO-SUB-ID-NUM 
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT 
           COMPUTE SUM-GRADE = SUM-GRADE + (NUM-GRADE * CREDIT)
           IF FIRST-SUB-ID-NUM = "3" THEN
              COMPUTE SUM-SCI-CREDIT = SUM-SCI-CREDIT + CREDIT
              COMPUTE SUM-SCI-GRADE = SUM-SCI-GRADE 
                + (NUM-GRADE * CREDIT) 
                IF TWO-SUB-ID-NUM = "31" THEN
                  COMPUTE SUM-CS-CREDIT = SUM-CS-CREDIT + CREDIT 
                  COMPUTE SUM-CS-GRADE = SUM-CS-GRADE 
                  + (NUM-GRADE * CREDIT)
                END-IF
           END-IF 
           .
       
       001-EXIT.
           EXIT
           .
       
       002-AVG-CAL.
           COMPUTE AVG-GRADE IN AVG-FILE = SUM-GRADE / SUM-CREDIT 
           COMPUTE AVG-SCI-GRADE 
              IN AVG-FILE = SUM-SCI-GRADE / SUM-SCI-CREDIT 
           COMPUTE AVG-CS-GRADE 
              IN AVG-FILE = SUM-CS-GRADE / SUM-CS-CREDIT 
           DISPLAY "AVG-GRADE = " AVG-GRADE IN AVG-DETAIL 
           DISPLAY "AVG-SCI-GRADE = " AVG-SCI-GRADE IN AVG-DETAIL  
           DISPLAY "AVG-CS-GRADE = " AVG-CS-GRADE IN AVG-DETAIL 
           WRITE AVG-DETAIL 
           .
       
       002-EXIT.
           EXIT
           .

            
